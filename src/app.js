'use strict';
let itemTaskCreateDOM = `
  <div class="item-task-content" draggable='true'>
    <button
      class="cursor-pointer transition duration-300 p-2 mx-2 hover:text-purple-300 rounded-lg w-auto"
    >
      : :
    </button>
    <input type="checkbox" class="item-checkbox" name="task" />
    <span class="hover:text-cyan-700 ml-3 text-lg font-medium text-justify">
    </span>
    <span class="flex-grow"></span>
    <button class="btn-remove"><i class='text-base md:text-xl bx bxs-trash bx-tada-hover'></i></button>
  </div>
`;

let inputTextDOM = document.getElementById('input-text');
let itemTaskDOM = document.getElementById('item-task');
let listTaskDOM = document.getElementById('item-list');

let listItems = [];
let listItemNode = [];

function updateListItem(items = listItems) {
  listTaskDOM.innerHTML = '';
  items.forEach((element) => {
    let createItem = document.createElement('li');
    createItem.className = 'item-task';
    createItem.setAttribute('data-index', element.id);
    createItem.innerHTML = itemTaskCreateDOM;
    let checkBoxDOM = createItem.querySelector('input.item-checkbox');
    let btnRemoveDOM = createItem.querySelector('button.btn-remove');
    let textDOM = createItem.querySelector('span');
    textDOM.innerHTML = element.text;

    if (element.completed) {
      textDOM.classList.add('line-through', 'text-zinc-400');
    }
    checkBoxDOM.checked = element.completed;
    checkBoxDOM.addEventListener('click', () => {
      changeCompleted(element.id);
    });

    btnRemoveDOM.addEventListener('click', () => {
      removeTask(element.id);
    });

    createItem.addEventListener('dragstart', dragStart);
    createItem.addEventListener('drop', dragDrop);
    createItem.addEventListener('dragover', dragOver);

    listTaskDOM.appendChild(createItem);
    updateListItemNode(createItem);
  });
}

function updateListItemNode(item) {
  let dataIndex = item.dataset.index;
  let existNode = listItemNode.find((i) => i.dataset.index === dataIndex);
  listItemNode = existNode ? listItemNode : [...listItemNode, item];
}

function removeTask(id) {
  let index = listItems.findIndex((item) => item.id == id);
  listItems.splice(index, 1);
  updateListItem(listItems);
}

function changeCompleted(id) {
  listItems = [
    ...listItems.map((item) =>
      item.id == id ? { ...item, completed: !item.completed } : item
    ),
  ];
  updateListItem(listItems);
}

let i = 1;
let dragStartIndex;

inputTextDOM.addEventListener('keyup', (e) => {
  if (e.key === 'Enter') {
    listItems.push({
      id: i++,
      text: e.target.value,
      completed: false,
    });

    e.target.value = '';
    updateListItem(listItems);
  }
});

function dragDrop() {
  let dragEndIndex = this.dataset.index;
  swapItems(dragStartIndex, dragEndIndex);
}

function dragStart() {
  dragStartIndex = this.closest('li').dataset.index;
}

function dragOver(e) {
  e.preventDefault();
}

function swapItems(fromIndex, toIndex) {
  // FIX: Indexs
  let itemOne = listItemNode[fromIndex - 1].querySelector(
    'div.item-task-content'
  );
  let itemTwo = listItemNode[toIndex - 1].querySelector(
    'div.item-task-content'
  );
  listItemNode[fromIndex - 1].appendChild(itemTwo);
  listItemNode[toIndex - 1].appendChild(itemOne);
}
