module.exports = {
  mode: 'jit',
  darkMode: 'class',
  content: ['./src/**/*.{html,js}'],
  theme: {
    container: {
      center: true,
      padding: '1.5rem',
    },
    extend: {},
  },
  variants: {},
  plugins: [],
};
